# demo-gitbook

#### 介绍
创建gitbook的通用demo，用于构建本地创作

#### 准备

1. git
2. node环境（npm安装）


#### 安装教程

1. 安装GitBook

```
npm install gitbook-cli -g
```

2.  Clone 代码到本地文件夹中

```
git clone https://gitee.com/gittenfist/demo-gitbook.git
```

3. 进入demo-gitbook运行命令,安装插件

```
gitbook install
```

4. 最后启动服务

```
gitbook serve
```

5. 在浏览器中打开  http://localhost:4000/  进行访问

#### 使用说明

1. 通过修改 book.json 设置标题、作者等，同时配置安装新的插件
2. 通过编辑 docs/SUMMARY.md 进行写作，gitbook自带插件支持热加载，写作完成无需重启服务

#### [预览](https://gittenfist.gitee.io/demo-gitbook)

